package ru.omsu.imit;

import java.util.Objects;

public class Student extends Human {
    private String University;
    private String Faculty;
    private String Specialty;


    public Student(
            String secondName,
            String firstName,
            String patronymic,
            int age,
            Human.Gender gender,
            String university,
            String faculty,
            String specialty
    )
    {
        super(secondName, firstName, patronymic, age, gender);
        University = university;
        Faculty = faculty;
        Specialty = specialty;
    }

    public String getUniversity() {
        return University;
    }

    public String getFaculty() {
        return Faculty;
    }

    public String getSpecialty() {
        return Specialty;
    }

    public void setUniversity(String university) {
        University = university;
    }

    public void setFaculty(String faculty) {
        Faculty = faculty;
    }

    public void setSpecialty(String specialty) {
        Specialty = specialty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        if (!super.equals(o)) return false;
        Student student = (Student) o;
        return getUniversity().equals(student.getUniversity()) &&
                getFaculty().equals(student.getFaculty()) &&
                getSpecialty().equals(student.getSpecialty());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getUniversity(), getFaculty(), getSpecialty());
    }
}

