package ru.omsu.imit;

import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class LambdaRunner {
    public static <T, R> R function(Function<T, R> lambda, T tObj) {
        return lambda.apply(tObj);
    }

    public static <T> T unaryOperator(UnaryOperator<T> lambda, T obj){
        return lambda.apply(obj);
    }

    public static <T> boolean predicate(Predicate<T> lambda, T tObj) {
        return lambda.test(tObj);
    }

    public static <T1, T2> boolean biPredicate(BiPredicate<T1, T2> lambda, T1 t1Obj, T2 t2Obj) {
        return lambda.test(t1Obj ,t2Obj);
    }

    public static <T1, T2, T3, T4> boolean fourTermsPredicate(
            FourTermsPredicate<T1, T2, T3, T4> lambda,
            T1 t1Obj,
            T2 t2Obj,
            T3 t3Obj,
            T4 t4Obj
    ){
        return lambda.test(t1Obj ,t2Obj, t3Obj ,t4Obj);
    }
}
