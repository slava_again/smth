package ru.omsu.imit;

import java.util.function.*;

public class LambdaDemo {
    // для строки символов получить ее длину
    public static final Function<String, Integer>
            stringLength = s -> {
        if (s == null) {
            throw new IllegalArgumentException("s == null");
        }

        return s.length();
    };

    // для строки символов получить ее первый символ, если он существует, или null иначе
    public static final Function<String, Character>
        firstChar = s -> {
      if(s == null){
          throw new IllegalArgumentException("The string is null");
      }
      if(s.length() == 0){
          return null;
      }

      return s.charAt(0);
    };

    //для строки проверить, что она не содержит пробелов
    public static final Predicate<String>
        doesNotContainSpace = s -> {
        if(s == null){
            throw new IllegalArgumentException("The string is null");
        }

        for(int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ' ') {
                return false;
            }
        }

        return true;
        //return !s.contains(" ");
    };

    //слова в строке разделены запятыми, по строке получить количество слов в ней
    public static final Function<String, Integer>
        numberOfWords = s -> {
        if(s == null){
            throw new IllegalArgumentException("The string is null");
        }
        int count = 1;
        for(int i = 0; i < s.length(); i++){
            if(s.charAt(i) == ','){
                count++;
            }
        }
        return count;
        //return s.split(",").length (+ 1)
    };

    //по человеку получить его возраст
    public static final Function<Human, Integer>
        theirAge = human -> {
        if(human == null){
            throw new IllegalArgumentException("The human is null");
        }
        return human.getAge();
    };

    //по двум людям проверить, что у них одинаковая фамилия
    public static final BiPredicate<Human, Human>
        doSurnamesCoincide = (human1, human2) -> human1.getSecondName().equals(human2.getSecondName());


    //получить фамилию, имя и отчество человека в виде одной строки (разделитель —
    //пробел)
    public static final Function<Human, String>
        getFullName = human -> {
        if(human == null){
            throw new IllegalArgumentException("The human is null");
        }
        if(human.getPatronymic().length() == 0){
            return human.getSecondName() + " " + human.getFirstName();
        }
        return human.getSecondName() + " " + human.getFirstName() + " " + human.getPatronymic();
    };

    //сделать человека старше на один год (по объекту Human создать новый объект)
    public static final UnaryOperator<Human>
        makeAOneYearOlder = human -> {
        return new Human(
                human.getSecondName(),
                human.getFirstName(),
                human.getPatronymic(),
                human.getAge() + 1,
                human.getGender()
        );
    };

    //по трем людям и заданному возрасту maxAge проверить, что все три человека моложе
    //maxAge
    public static final FourTermsPredicate<Human, Human, Human, Integer>
        areAllYoungerThan = (human1, human2, human3, maxAge) -> {
        if(human1 == null){
            throw new IllegalArgumentException("The human1 is null");
        }
        if(human2 == null){
            throw new IllegalArgumentException("The human2 is null");
        }
        if(human3 == null){
            throw new IllegalArgumentException("The human3 is null");
        }
        if(maxAge == null){
            throw new IllegalArgumentException("The maxAge is null");
        }
        return (human1.getAge() < maxAge && human2.getAge() < maxAge && human3.getAge() < maxAge);
    };


















}
