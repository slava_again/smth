package ru.omsu.imit;

import java.util.Objects;

public class Human {
    private String SecondName;
    private String FirstName;
    private String Patronymic;
    private int Age;
    private Human.Gender gender;

    public Human(Human human) {
        SecondName = human.SecondName;
        FirstName = human.FirstName;
        Patronymic = human.Patronymic;
        Age = human.Age;
        this.gender = human.gender;
    }

    public enum Gender{
        Male,
        Female;
    }

    public Human(String secondName, String firstName, String patronymic, int age, Gender gender) {
        SecondName = secondName;
        FirstName = firstName;
        Patronymic = patronymic;
        Age = age;
        this.gender = gender;
    }

    public String getSecondName() {
        return SecondName;
    }

    public void setSecondName(String secondName) {
        SecondName = secondName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getPatronymic() {
        return Patronymic;
    }

    public void setPatronymic(String patronymic) {
        Patronymic = patronymic;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return getAge() == human.getAge() &&
                getSecondName().equals(human.getSecondName()) &&
                getFirstName().equals(human.getFirstName()) &&
                getPatronymic().equals(human.getPatronymic()) &&
                getGender() == human.getGender();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSecondName(), getFirstName(), getPatronymic(), getAge(), getGender());
    }
}
