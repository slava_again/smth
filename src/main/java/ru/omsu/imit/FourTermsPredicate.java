package ru.omsu.imit;

@FunctionalInterface
public interface FourTermsPredicate<T1, T2, T3, T4> {
    boolean test(T1 obj1, T2 obj2, T3 obj3, T4 obj4);
}
