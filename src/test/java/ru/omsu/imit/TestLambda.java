package ru.omsu.imit;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class TestLambda {
    private static final Student student1 = new Student(
            "Задворнов", "Вячеслав", "Сергеевич",
            20, Human.Gender.Male, "ОмГУ", "ИМИТ", "Прикладная математика");

    private static final Student student2 = new Student(
            "Задворнов", "Вячеслав", "Сергеевич", 18, Human.Gender.Male,
            "НГУ", "ММФ", "Математика");

    private static final Human human1 = new Human(
            "Laurent", "Pierre", "Alphonse", 207, Human.Gender.Male);

    private static final Human human2 = new Human(
            "Euler", "Leonhard", "", 314, Human.Gender.Male);

    @Test
    public void testStringLength() {
        int actual = LambdaRunner.function(
                LambdaDemo.stringLength, "We're on drugs");

        Assertions.assertEquals(14, actual);

        actual = LambdaRunner.function(
                LambdaDemo.stringLength, "Hi, Phineas");

        Assertions.assertEquals(11, actual);

        actual = LambdaRunner.function(
                LambdaDemo.stringLength, "LOOOOOOOOOOOOL");

        Assertions.assertEquals(14, actual);
        Assertions.assertEquals(0, LambdaRunner.function(LambdaDemo.stringLength, ""));
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> LambdaRunner.function(LambdaDemo.stringLength, null)
        );
    }

    @Test
    public void testFirstCharOfString() {
        Character actual = LambdaRunner.function(
                LambdaDemo.firstChar, "Those were the days of our lives");

        Assertions.assertEquals(Character.valueOf('T'), actual);

        actual = LambdaRunner.function(
                LambdaDemo.firstChar, "");

        Assertions.assertNull(actual);
    }

    @Test
    public void testDoesNotContainSpaces() {
        boolean actual = LambdaRunner.predicate(
                LambdaDemo.doesNotContainSpace, "Every single moment");

        Assertions.assertFalse(actual);

        actual = LambdaRunner.predicate(
                LambdaDemo.doesNotContainSpace, "EverySingleBear");

        Assertions.assertTrue(actual);
    }

    @Test
    public void testNumberOfWordsInString() {
        int actual = LambdaRunner.function(
                LambdaDemo.numberOfWords,
                "Introduction, to, the, theory, of, the, functions, of, one, complex, argument"
        );

        Assertions.assertEquals(11, actual);

        actual = LambdaRunner.function(
                LambdaDemo.numberOfWords, "Those, equations, are, not, compatible");

        Assertions.assertEquals(5, actual);

        actual = LambdaRunner.function(
                LambdaDemo.numberOfWords, "");

        Assertions.assertEquals(1, actual);
    }

    @Test
    public void testHumanAge() {
        int actual = LambdaRunner.function(
                LambdaDemo.theirAge, student1);

        Assertions.assertEquals(student1.getAge(), actual);
    }

    @Test
    public void testEqualLastNames() {
        boolean actual = LambdaRunner.biPredicate(
                LambdaDemo.doSurnamesCoincide, student1, student2);

        Assertions.assertTrue(actual);

        actual = LambdaRunner.biPredicate(
                LambdaDemo.doSurnamesCoincide, human1, human2);

        Assertions.assertFalse(actual);
    }

    @Test
    public void testHumanFullName() {
        String actual = LambdaRunner.function(
                LambdaDemo.getFullName, student1);

        Assertions.assertEquals("Задворнов Вячеслав Сергеевич", actual);
    }

    @Test
    public void testHumanOlderBy1Year() {
        Human expected = new Human(human1);

        expected.setAge(expected.getAge() + 1);

        Human actual = LambdaRunner.function(
                LambdaDemo.makeAOneYearOlder, human1);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testThreeHumanAreYoungerThan() {
        boolean actual = LambdaRunner.fourTermsPredicate(
                LambdaDemo.areAllYoungerThan,
                student1, student2, human1, 400);

        Assertions.assertTrue(actual);

        actual = LambdaRunner.fourTermsPredicate(
                LambdaDemo.areAllYoungerThan,
                student1, student2, human2, 300);

        Assertions.assertFalse(actual);
    }
}
